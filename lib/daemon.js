var http = require('http');
var cp = require('child_process');
var events = require('events');
var request = require('request')

var async = require('async');


/**
 * The daemon interface interacts with the coin daemon by using the rpc interface.
 * in order to make it work it needs, as constructor, an array of objects containing
 * - 'host'    : hostname where the coin lives
 * - 'port'    : port where the coin accepts rpc connections
 * - 'user'    : username of the coin for the rpc interface
 * - 'password': password for the rpc interface of the coin

 * @param daemons - array of daemons
 * @param logger winston logger
 * @constructor
 */
function DaemonInterface(daemons, logger) {

    if (!logger) {
        throw new Error("No logger passed to DaemonInterface");
    }

    //private members
    var _this = this;

    var instances = (function () {
        for (var i = 0; i < daemons.length; i++)
            daemons[i]['index'] = i;
        return daemons;
    })();


    function init() {
        isOnline(function (online) {
            if (online)
                _this.emit('online');
        });
    }

    function isOnline(callback) {
        cmd('getblockcount', [], function (results) {
            var allOnline = results.every(function (result) {
                return !results.error;
            });
            callback(allOnline);
            if (!allOnline)
                _this.emit('connectionFailed', results);
        });
    }


    function performHttpRequest(instance, jsonData, method, callback) {
        // var method = JSON.parse(jsonData).method
        // console.log('jsonData.method', method)

        var options = {
            hostname: (typeof(instance.host) === 'undefined' ? '127.0.0.1' : instance.host),
            port: instance.port,
            method: 'POST',
            auth: instance.user + ':' + instance.password,
            headers: {
                'Content-Length': jsonData.length
            }
        };

        var parseJson = function (res, data) {
            var dataJson;

            if (res.statusCode === 401) {
                logger.error('Unauthorized RPC access - invalid RPC username or password');
                return;
            }

            if (res.statusCode !== 200) {
                logger.warn('Daemon returned unexpected return code %s, method = %s, data = %s', res.statusCode, method, JSON.stringify(data));
                return;
            }

            try {
                dataJson = JSON.parse(data);
            }
            catch (e) {
                if (data.indexOf(':-nan') !== -1) {
                    data = data.replace(/:-nan,/g, ":0");
                    parseJson(res, data);
                    return;
                }
                logger.error('Could not parse rpc data from daemon instance %s ' +
                    '\nRequest Data: %s' +
                    '\nReponse Data: %s',
                    instance.index,
                    jsonData,
                    data);
            }
            if (dataJson)
                callback(dataJson.error, dataJson, data);
        };

        var req = http.request(options, function (res) {
            var data = '';
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                data += chunk;
            });
            res.on('end', function () {
                parseJson(res, data);
            });
        });

        req.on('error', function (e) {
            if (e.code === 'ECONNREFUSED')
                callback({type: 'offline', message: e.message}, null);
            else
                callback({type: 'request error', message: e.message}, null);
        });

        req.end(jsonData);
    }


    //Performs a batch JSON-RPC command - only uses the first configured rpc daemon
    /* First argument must have:
     [
         [ methodName, [params] ],
         [ methodName, [params] ]
     ]
     */

    function batchCmd(cmdArray, callback) {

        var requestJson = [];

        for (var i = 0; i < cmdArray.length; i++) {
            requestJson.push({
                method: cmdArray[i][0],
                params: cmdArray[i][1],
                id: Date.now() + Math.floor(Math.random() * 10) + i
            });
        }
        var method = requestJson[0].method

        var serializedRequest = JSON.stringify(requestJson);

        performHttpRequest(instances[0], serializedRequest, method, function (error, result) {
            callback(error, result);
        });

    }

    /* Sends a JSON RPC (http://json-rpc.org/wiki/specification) command to every configured daemon.
       The callback function is fired once with the result from each daemon unless streamResults is
       set to true. */
    function cmd(method, params, callback, streamResults, returnRawData) {

        var results = [];

        async.each(instances, function (instance, eachCallback) {

            var itemFinished = function (error, result, data) {

                var returnObj = {
                    error: error,
                    response: (result || {}).result,
                    instance: instance
                };
                if (returnRawData) returnObj.data = data;
                if (streamResults) callback(returnObj);
                else results.push(returnObj);
                eachCallback();
                itemFinished = function () {
                };
            };

            var requestJson = JSON.stringify({
                method: method,
                params: params,
                id: Date.now() + Math.floor(Math.random() * 10)
            });

            performHttpRequest(instance, requestJson, method, function (error, result, data) {
                itemFinished(error, result, data);
            });


        }, function () {
            if (!streamResults) {
                callback(results);
            }
        });

    }

    function urlFx(instanceConfig) {
        var obj = {
            hostname: (typeof(instanceConfig.host) === 'undefined' ? '127.0.0.1' : instanceConfig.host),
            port: instanceConfig.port,
            auth: instanceConfig.user + ':' + instanceConfig.password
        }
        return `http://${obj.auth}@${obj.hostname}:${obj.port}`
    };

    function performHttpRequestPromise(instance, jsonData) {

        var options = {
            url: urlFx(instance),
            method: 'POST',
            headers: {
                'Content-Length': jsonData.length
            },
            body: jsonData,
            json: true
        };

        return new Promise((resolve, reject) => {

            request(options, function (error, response, body) {
                // console.log(body)
                if (response){
                    if (body.result)
                        return resolve(body.result)
                    else
                        return reject(body.error)
                }
                else
                    return reject(error)
            });
        })
    }

    function cmdResult(method, params, callback, streamResults, returnRawData) {

        var requestJson = {
            method: method,
            params: params,
            id: Date.now() + Math.floor(Math.random() * 10)
        };

        return new Promise((resolve, reject) => {
            // console.log('instances', instances.length)
            Promise.all(instances.map(i => performHttpRequestPromise(i, requestJson)))
                .then(results => {
                    return resolve(results)
                })
                .catch(err => {
                    return reject(err)
                })
        })
    }

    function cmdExist(method, params, callback, streamResults, returnRawData) {

        var requestJson = {
            method: method,
            params: params,
            id: Date.now() + Math.floor(Math.random() * 10)
        };

        return new Promise((resolve, reject) => {
            Promise.all(instances.map(i => performHttpRequestPromise(i, requestJson)))
                .then(results => {
                    return resolve(results)
                })
                .catch(err => {
                    return reject(err)
                })
        })
    }


    //public members

    this.init = init;
    this.isOnline = isOnline;
    this.cmd = cmd;
    this.cmdResult = cmdResult;
    this.cmdExist = cmdExist;
    this.batchCmd = batchCmd;
}

DaemonInterface.prototype.__proto__ = events.EventEmitter.prototype;

exports.interface = DaemonInterface;